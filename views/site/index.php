<?php

/** @var yii\web\View $this */

$this->title = 'Escuela StepUp';
?>

<div class="site-index">
           
    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4"><strong>¡¡Bienvenido a la escuela StepUp!!</strong></h1>

        <h2 class="lead">En nuestra escuela te brindamos la oportunidad de dar a conocer tu arte bailando</h2>
        <h2 class="lead">Si estas interesado en conocernos, te ofrecemos la opción de matricularte en nuestra escuela</h2>
        <h2 class="lead">Pulsa a continuación para introducir tus datos y nos pondremos en contacto contigo</h2>

        <p><a class="btn btn-lg btn-success" href="http://localhost/dam/schoolbaile/web/index.php/alumnos/create"><strong>Let's Dance</strong></a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2><strong>Equipo Docente</strong></h2>

                <p>En nuestra escuela disponemos de un gran equipo de maestros en diferentes estilos. Gracias a 
                   estos docentes podrás desarrollar la coordinación, el ritmo, la musicalidad, la agilidad, la
                   flexibilidad, la orientación espacial, la interpretación y el trabajo en equipo.</p>
                
                <p><a class="btn btn-outline-secondary" href="http://localhost/dam/schoolbaile/web/index.php/profesores/index"><strong>Profesores del Centro &raquo;</strong></a></p>
            </div>
            <div class="col-lg-4">
                <h2><strong>Estilos de Baile</strong></h2>

                <p>En nuestra escuela, se te dara la oportunidad de aprender 4 tipos distintos de baile. Principalmente,
                   nos centramos en los estilos: Clásico, Latino, Moderno y Urbano. Dentro de cada uno de estos 4 grupos,
                   podrás recibir clases de los diferentes subtipos que el equipo docente domina.</p>
                
                <p><a class="btn btn-outline-secondary" href="http://localhost/dam/schoolbaile/web/index.php/estilos/index"><strong>Estilos Impartidos &raquo;</strong></a></p>
            </div>
            <div class="col-lg-4">
                <h2><strong>Categorías de Baile</strong></h2>

                <p>A parte de cada uno de los estilos que ofrecemos, seremos capaces de prepararte para participar en las
                    diferentes categorías que existen en los diversos campeonatos que se organizarán cada año. Dependiendo
                    de la edad que tengas, podrás participar en 4 categorías distintas</p>
                <p><a class="btn btn-outline-secondary" href="http://localhost/dam/schoolbaile/web/index.php/categorias/index"><strong>Categorías de campeonato &raquo;</strong></a></p>                
            </div>
        </div>

    </div>
</div>
