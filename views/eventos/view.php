<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Eventos $model */

$this->title = $model->nombre_evento;
$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="eventos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar Evento', ['update', 'nombre_evento' => $model->nombre_evento], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar Evento', ['delete', 'nombre_evento' => $model->nombre_evento], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Desea no incluir los datos de este evento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre_evento',
            'fecha',
            'tipo_evento',
            'lugar_establecimiento',
        ],
    ]) ?>

</div>
