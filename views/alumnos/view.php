<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Alumnos $model */

$this->title = $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Alumnos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="alumnos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar alumno', ['update', 'dni' => $model->dni], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar alumno', ['delete', 'dni' => $model->dni], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Desea no incluir los datos de este alumno?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dni',
            'nombre',
            'apellidos',
            'f_nac',
            'direccion',
            'telefono',
            'estilo_baile',
            //'nombre_categoria',
        ],
    ]) ?>

</div>
