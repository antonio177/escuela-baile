<?php

use app\models\Existen;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Existen';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="existen-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Creación de Existen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre_evento',
            'nombre_categoria',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Existen $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
