<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Existen $model */

$this->title = 'Creación de Existen';
$this->params['breadcrumbs'][] = ['label' => 'Existen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="existen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
