<?php

use app\models\Estilos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Estilos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estilos-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'tipo',
            'subtipo',
            'num_alumnos',
            /*[
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Estilos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'tipo' => $model->tipo]);
                 }
            ],*/
        ],
    ]); ?>


</div>
