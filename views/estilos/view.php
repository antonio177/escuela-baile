<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Estilos $model */

$this->title = $model->tipo;
$this->params['breadcrumbs'][] = ['label' => 'Estilos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="estilos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar EStilo', ['update', 'tipo' => $model->tipo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar Estilo', ['delete', 'tipo' => $model->tipo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Desea no incluir los datos de este estilo?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tipo',
            'subtipo',
            'num_alumnos',
        ],
    ]) ?>

</div>
