<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Estilos $model */

$this->title = 'Update Estilos: ' . $model->tipo;
$this->params['breadcrumbs'][] = ['label' => 'Estilos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tipo, 'url' => ['view', 'tipo' => $model->tipo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estilos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
