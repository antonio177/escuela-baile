<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Estilos $model */

$this->title = 'Creción de Estilos';
$this->params['breadcrumbs'][] = ['label' => 'Estilos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estilos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
