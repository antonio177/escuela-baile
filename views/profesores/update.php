<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Profesores $model */

$this->title = 'Update Profesores: ' . $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Profesores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni, 'url' => ['view', 'dni' => $model->dni]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="profesores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
