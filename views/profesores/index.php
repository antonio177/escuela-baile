<?php

use app\models\Profesores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Profesores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesores-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
       

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'dni',
            'nombre',
            'apellidos',
            'direccion',
            'estilo_impartido',
            'tipo_estilo',
            /*[
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Profesores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'dni' => $model->dni]);
                 }
            ],*/
        ],
    ]); ?>


</div>
