<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Eligen $model */

$this->title = 'Creación de Eligen';
$this->params['breadcrumbs'][] = ['label' => 'Eligen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eligen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>

</div>
