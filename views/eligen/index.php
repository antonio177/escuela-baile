<?php

use app\models\Eligen;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Eligen';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eligen-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Creación de Eligen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'dni_alumnos',
            'tipo_estilo',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Eligen $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
