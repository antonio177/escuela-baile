<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/** @var yii\web\View $this */
/** @var app\models\Eligen $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="eligen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni_alumnos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_estilo')->widget(Select2::classname(), [
            'data' => $data,
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); 
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar datos', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
