<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Eligen $model */

$this->title = 'Update Eligen: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Eligen', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="eligen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
