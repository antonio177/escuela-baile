<?php

namespace app\controllers;

use app\models\Estilos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * EstilosController implements the CRUD actions for Estilos model.
 */
class EstilosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Estilos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Estilos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'tipo' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Estilos model.
     * @param string $tipo Tipo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($tipo)
    {
        return $this->render('view', [
            'model' => $this->findModel($tipo),
        ]);
    }

    /**
     * Creates a new Estilos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Estilos();
        $data = ArrayHelper::map(Estilos::find()->all(), 'tipo', 'tipo');
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'tipo' => $model->tipo]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Updates an existing Estilos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $tipo Tipo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($tipo)
    {
        $model = $this->findModel($tipo);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'tipo' => $model->tipo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Estilos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $tipo Tipo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($tipo)
    {
        $this->findModel($tipo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Estilos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $tipo Tipo
     * @return Estilos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($tipo)
    {
        if (($model = Estilos::findOne(['tipo' => $tipo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
