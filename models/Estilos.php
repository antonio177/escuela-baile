<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estilos".
 *
 * @property string $tipo
 * @property string|null $subtipo
 * @property int|null $num_alumnos
 *
 * @property Alumnos[] $dniAlumnos
 * @property Eligen[] $eligens
 * @property Profesores[] $profesores
 */
class Estilos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estilos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo','subtipo','num_alumnos'], 'required'],
            [['num_alumnos'], 'integer'],
            [['num_alumnos'], 'match', 'pattern' => "/^[0-9]+$/", 'message' => 'Sólo se aceptan números.'],
            [['tipo', 'subtipo'], 'string', 'max' => 50],
            //[['tipo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tipo' => 'Tipo de Baile',
            'subtipo' => 'Subtipo',
            'num_alumnos' => 'Número de Alumnos',
        ];
    }

    /**
     * Gets query for [[DniAlumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniAlumnos()
    {
        return $this->hasMany(Alumnos::class, ['dni' => 'dni_alumnos'])->viaTable('eligen', ['tipo_estilo' => 'tipo']);
    }

    /**
     * Gets query for [[Eligens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEligens()
    {
        return $this->hasMany(Eligen::class, ['tipo_estilo' => 'tipo']);
    }

    /**
     * Gets query for [[Profesores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfesores()
    {
        return $this->hasMany(Profesores::class, ['tipo_estilo' => 'tipo']);
    }
}
