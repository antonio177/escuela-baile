<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "existen".
 *
 * @property int $id
 * @property string|null $nombre_evento
 * @property string|null $nombre_categoria
 *
 * @property Categorias $nombreCategoria
 * @property Eventos $nombreEvento
 */
class Existen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'existen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_evento','nombre_categoria'], 'required'],
            [['nombre_evento', 'nombre_categoria'], 'string', 'max' => 30],
            [['nombre_evento'], 'match', 'pattern' => "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÑñÙÄËÏÖÜ]+$/", 'message' => 'Sólo se aceptan letras.'],
            [['nombre_evento', 'nombre_categoria'], 'unique', 'targetAttribute' => ['nombre_evento', 'nombre_categoria']],
            [['nombre_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::class, 'targetAttribute' => ['nombre_categoria' => 'nombre']],
            [['nombre_evento'], 'exist', 'skipOnError' => true, 'targetClass' => Eventos::class, 'targetAttribute' => ['nombre_evento' => 'nombre_evento']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre_evento' => 'Nombre del Evento',
            'nombre_categoria' => 'Nombre de la Categoría',
        ];
    }

    /**
     * Gets query for [[NombreCategoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreCategoria()
    {
        return $this->hasOne(Categorias::class, ['nombre' => 'nombre_categoria']);
    }

    /**
     * Gets query for [[NombreEvento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreEvento()
    {
        return $this->hasOne(Eventos::class, ['nombre_evento' => 'nombre_evento']);
    }
}
