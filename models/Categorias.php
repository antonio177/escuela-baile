<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categorias".
 *
 * @property string $nombre
 * @property string|null $tipo_baile
 * @property int|null $num_participantes
 * @property string|null $dni_alumno
 *
 * @property Alumnos[] $alumnos
 * @property Existen[] $existens
 * @property Eventos[] $nombreEventos
 */
class Categorias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre','num_participantes','tipo_baile','dni_alumno'], 'required'],
            [['num_participantes'], 'integer'],
            [['num_participantes'], 'match', 'pattern' => "/^[0-9]+$/", 'message' => 'Sólo se aceptan números.'],
            [['nombre'], 'string', 'max' => 30],
            [['nombre'], 'match', 'pattern' => "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÑñÙÄËÏÖÜ]+$/", 'message' => 'Sólo se aceptan letras.'],
            [['tipo_baile'], 'string', 'max' => 50],
            [['dni_alumno'], 'string', 'min' => 9, 'max' => 9],
            [['dni_alumno'], 'match', 'pattern' => "/^[0-9A-ZÑ]+$/", 'message' => 'Inserte un formato válido para el dni.'],
            [['nombre','dni_alumno'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'tipo_baile' => 'Tipo de Baile',
            'num_participantes' => 'Número de Participantes',
            'dni_alumno' => 'DNI del Alumno',
        ];
    }

    /**
     * Gets query for [[Alumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlumnos()
    {
        return $this->hasMany(Alumnos::class, ['nombre_categoria' => 'nombre']);
    }

    /**
     * Gets query for [[Existens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExistens()
    {
        return $this->hasMany(Existen::class, ['nombre_categoria' => 'nombre']);
    }

    /**
     * Gets query for [[NombreEventos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreEventos()
    {
        return $this->hasMany(Eventos::class, ['nombre_evento' => 'nombre_evento'])->viaTable('existen', ['nombre_categoria' => 'nombre']);
    }
}
