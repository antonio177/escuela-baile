<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $f_nac
 * @property string|null $direccion
 * @property string|null $telefono
 * @property string|null $estilo_baile
 * @property string|null $nombre_categoria
 *
 * @property Eligen[] $eligens
 * @property Categorias $nombreCategoria
 * @property Estilos[] $tipoEstilos
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni','nombre','apellidos','f_nac','direccion','telefono','estilo_baile'], 'required'],
            [['f_nac'], 'safe'],
            [['f_nac'], 'compare', 'compareValue' => date('Y-m-d'), 'operator' => '<=', 'message' => 'La fecha no puede ser posterior al día actual.'],
            [['dni'], 'string','min' => 9, 'max' => 9],
            [['dni'], 'match', 'pattern' => "/^[0-9A-ZÑ]+$/", 'message' => 'Inserte un formato válido para el dni.'],
            [['nombre'], 'string', 'max' => 20],
            [['nombre'], 'match', 'pattern' => "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÑñÙÄËÏÖÜ]+$/", 'message' => 'Sólo se aceptan letras.'],
            [['apellidos', 'direccion', 'estilo_baile'], 'string', 'max' => 50],
            [['apellidos'], 'match', 'pattern' => "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÑñÙÄËÏÖÜ\s]+$/", 'message' => 'Sólo se aceptan letras.'],
            [['direccion'], 'match', 'pattern' => "/^[0-9a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÑñÙÄËÏÖÜ\s]+$/", 'message' => 'Sólo se aceptan letras y números.'],
            [['telefono'], 'string', 'min' => 9, 'max' => 9, 'message' => 'Introduce 9 dígitos de tu número de teléfono'],
            [['telefono'], 'match', 'pattern' => "/^[0-9]+$/", 'message' => 'Sólo se aceptan números.'],            
            //[['nombre_categoria'], 'string', 'max' => 30],
            [['dni','telefono'], 'unique'],
            //[['nombre_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::class, 'targetAttribute' => ['nombre_categoria' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'DNI',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'f_nac' => 'Fecha Nacimiento',
            'direccion' => 'Dirección',
            'telefono' => 'Teléfono',
            'estilo_baile' => 'Estilo Baile',
            //'nombre_categoria' => 'Nombre Categoria',
        ];
    }

    /**
     * Gets query for [[Eligens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEligens()
    {
        return $this->hasMany(Eligen::class, ['dni_alumnos' => 'dni']);
    }

    /**
     * Gets query for [[NombreCategoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreCategoria()
    {
        return $this->hasOne(Categorias::class, ['nombre' => 'nombre_categoria']);
    }

    /**
     * Gets query for [[TipoEstilos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoEstilos()
    {
        return $this->hasMany(Estilos::class, ['tipo' => 'tipo_estilo'])->viaTable('eligen', ['dni_alumnos' => 'dni']);
    }
}
