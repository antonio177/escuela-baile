<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $id
 * @property string|null $telefono_profesor
 * @property string|null $dni_profesor
 *
 * @property Profesores $dniProfesor
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefono_profesor','dni_profesor'], 'required'],
            [['telefono_profesor'], 'string', 'min' => 9, 'max' => 9],
            [['telefono'], 'match', 'pattern' => "/^[0-9]+$/", 'message' => 'Sólo se aceptan números.'],
            [['dni_profesor'], 'string','min' => 9, 'max' => 9],
            [['dni_profesor'], 'match', 'pattern' => "/^[0-9A-ZÑ]+$/", 'message' => 'Inserte un formato válido para el dni.'],
            [['telefono_profesor', 'dni_profesor'], 'unique', 'targetAttribute' => ['telefono_profesor', 'dni_profesor']],
            [['dni_profesor'], 'exist', 'skipOnError' => true, 'targetClass' => Profesores::class, 'targetAttribute' => ['dni_profesor' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telefono_profesor' => 'Teléfono del Profesor',
            'dni_profesor' => 'DNI del Profesor',
        ];
    }

    /**
     * Gets query for [[DniProfesor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniProfesor()
    {
        return $this->hasOne(Profesores::class, ['dni' => 'dni_profesor']);
    }
}
