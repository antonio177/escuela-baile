<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "eligen".
 *
 * @property int $id
 * @property string|null $dni_alumnos
 * @property string|null $tipo_estilo
 *
 * @property Alumnos $dniAlumnos
 * @property Estilos $tipoEstilo
 */
class Eligen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'eligen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni_alumnos','tipo_estilo'], 'required'],
            [['dni_alumnos'], 'string', 'min' => 9, 'max' => 9],
            [['dni_alumnos'], 'match', 'pattern' => "/^[0-9A-ZÑ]+$/", 'message' => 'Inserte un formato válido para el dni.'],
            [['tipo_estilo'], 'string', 'max' => 50],
            [['dni_alumnos'], 'unique', 'targetAttribute' => ['dni_alumnos', 'tipo_estilo']],
            [['dni_alumnos'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::class, 'targetAttribute' => ['dni_alumnos' => 'dni']],
            [['tipo_estilo'], 'exist', 'skipOnError' => true, 'targetClass' => Estilos::class, 'targetAttribute' => ['tipo_estilo' => 'tipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dni_alumnos' => 'DNI del Alumno',
            'tipo_estilo' => 'Tipo de Estilo',
        ];
    }

    /**
     * Gets query for [[DniAlumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniAlumnos()
    {
        return $this->hasOne(Alumnos::class, ['dni' => 'dni_alumnos']);
    }

    /**
     * Gets query for [[TipoEstilo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoEstilo()
    {
        return $this->hasOne(Estilos::class, ['tipo' => 'tipo_estilo']);
    }
}
