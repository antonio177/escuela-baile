<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "eventos".
 *
 * @property string $nombre_evento
 * @property string|null $fecha
 * @property string|null $tipo_evento
 * @property string|null $lugar_establecimiento
 *
 * @property Existen[] $existens
 * @property Categorias[] $nombreCategorias
 */
class Eventos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'eventos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_evento','fecha','nombre_evento','lugar_establecimiento'], 'required'],
            [['fecha'], 'safe'],
            [['fecha'], 'compare', 'compareValue' => date('Y-m-d'), 'operator' => '<=', 'message' => 'La fecha no puede ser posterior al día actual.'],
            [['nombre_evento', 'tipo_evento'], 'string', 'max' => 30],
            [['lugar_establecimiento'], 'string', 'max' => 50],
            [['lugar_establecimiento'], 'match', 'pattern' => "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÑñÙÄËÏÖÜ\s]+$/", 'message' => 'Sólo se aceptan letras.'],
            [['nombre_evento'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre_evento' => 'Nombre del Evento',
            'fecha' => 'Fecha del Evento',
            'tipo_evento' => 'Tipo de Evento',
            'lugar_establecimiento' => 'Lugar de Establecimiento',
        ];
    }

    /**
     * Gets query for [[Existens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExistens()
    {
        return $this->hasMany(Existen::class, ['nombre_evento' => 'nombre_evento']);
    }

    /**
     * Gets query for [[NombreCategorias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreCategorias()
    {
        return $this->hasMany(Categorias::class, ['nombre' => 'nombre_categoria'])->viaTable('existen', ['nombre_evento' => 'nombre_evento']);
    }
}
