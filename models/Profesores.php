<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profesores".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $direccion
 * @property string|null $estilo_impartido
 * @property string|null $tipo_estilo
 *
 * @property Telefonos[] $telefonos
 * @property Estilos $tipoEstilo
 */
class Profesores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profesores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni','nombre','apellidos','direccion','direccion','estilo_impartido'], 'required'],
            [['dni'], 'string', 'min' => 9, 'max' => 9],
            [['dni'], 'match', 'pattern' => "/^[0-9A-ZÑ]+$/", 'message' => 'Inserte un formato válido para el dni.'],
            [['nombre'], 'string', 'max' => 20],
            [['nombre'], 'match', 'pattern' => "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÑñÙÄËÏÖÜ]+$/", 'message' => 'Sólo se aceptan letras.'],
            [['apellidos', 'direccion', 'tipo_estilo'], 'string', 'max' => 50],
            [['apellidos'], 'match', 'pattern' => "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÑñÙÄËÏÖÜ\s]+$/", 'message' => 'Sólo se aceptan letras.'],
            [['direccion'], 'match', 'pattern' => "/^[0-9a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÑñÙÄËÏÖÜ\s]+$/", 'message' => 'Sólo se aceptan letras y números.'],
            [['estilo_impartido'], 'string', 'max' => 40],
            [['dni'], 'unique'],
            [['tipo_estilo'], 'exist', 'skipOnError' => true, 'targetClass' => Estilos::class, 'targetAttribute' => ['tipo_estilo' => 'tipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'DNI',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'direccion' => 'Dirección',
            'estilo_impartido' => 'Estilo Impartido',
            'tipo_estilo' => 'Tipo de Estilo',
        ];
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::class, ['dni_profesor' => 'dni']);
    }

    /**
     * Gets query for [[TipoEstilo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoEstilo()
    {
        return $this->hasOne(Estilos::class, ['tipo' => 'tipo_estilo']);
    }
}
